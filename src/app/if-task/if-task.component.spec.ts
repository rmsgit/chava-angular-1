import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IfTaskComponent } from './if-task.component';

describe('IfTaskComponent', () => {
  let component: IfTaskComponent;
  let fixture: ComponentFixture<IfTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IfTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IfTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
