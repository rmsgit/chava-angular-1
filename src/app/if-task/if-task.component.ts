import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-if-task',
  templateUrl: './if-task.component.html',
  styleUrls: ['./if-task.component.css']
})
export class IfTaskComponent implements OnInit {


  isShow: boolean = false;


  list: Array<number> = [
    20, 50, 420, 80
  ]

  constructor() { }

  ngOnInit() {
  }

  onClick(){
    this.isShow = !this.isShow;

    this.list.push(Math.random())
  }

}
