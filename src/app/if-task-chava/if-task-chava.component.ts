import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-if-task-chava',
  templateUrl: './if-task-chava.component.html',
  styleUrls: ['./if-task-chava.component.css']
})
export class IfTaskChavaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  isShow: boolean = false;

  list: Array<number> = [20, 85, 65, 63];

  onclick(){
    this.isShow=!this.isShow;

    this.list.push(Math.random())
  }

}
