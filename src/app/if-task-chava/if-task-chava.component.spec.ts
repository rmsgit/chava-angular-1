import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IfTaskChavaComponent } from './if-task-chava.component';

describe('IfTaskChavaComponent', () => {
  let component: IfTaskChavaComponent;
  let fixture: ComponentFixture<IfTaskChavaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IfTaskChavaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IfTaskChavaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
