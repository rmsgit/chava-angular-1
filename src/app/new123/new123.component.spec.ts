import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { New123Component } from './new123.component';

describe('New123Component', () => {
  let component: New123Component;
  let fixture: ComponentFixture<New123Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ New123Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(New123Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
