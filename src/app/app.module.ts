import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { New123Component } from './new123/new123.component';
import { AluthComponent } from './aluth/aluth.component';
import { ParanaComponent } from './parana/parana.component';
import { IfTaskComponent } from './if-task/if-task.component';
import { IfTaskChavaComponent } from './if-task-chava/if-task-chava.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    New123Component,
    AluthComponent,
    ParanaComponent,
    IfTaskComponent,
    IfTaskChavaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
