import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aluth',
  templateUrl: './aluth.component.html',
  styleUrls: ['./aluth.component.css']
})

export class AluthComponent implements OnInit {

  public title: string = "this is from var";

  constructor() {
  
  }

  ngOnInit() {
  }

  test(num: number): string {
    return "Title - " + num;
  }

  onClick(){
    this.title = this.test(Math.random())
  }

}
