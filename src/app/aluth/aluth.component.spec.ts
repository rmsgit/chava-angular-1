import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AluthComponent } from './aluth.component';

describe('AluthComponent', () => {
  let component: AluthComponent;
  let fixture: ComponentFixture<AluthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AluthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AluthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
